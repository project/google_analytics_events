<?php

namespace Drupal\google_analytics_events;

class Event {

  /**
   * Unique id for this event.
   *
   * @var string
   */
  private $id;

  /**
   * Storage for the metric dimensions.
   *
   * @var mixed[]
   */
  protected $dimensions = [];

  public function __construct() {
    $this->id = uniqid(microtime(TRUE), TRUE);
  }

  /**
   * Validate the length of a string in bytes.
   *
   * @param string $data
   *   Data to verify.
   * @param int $length
   *   Maximum length in bytes.
   * @throws \Exception
   */
  protected function validateLength($data, $length) {
    if (mb_strlen($data, '8bit') > $length) {
      throw new \InvalidArgumentException("Invalid parameter length.");
    }
  }

  /**
   * Summary of setHitType
   *
   * @param string $type
   */
  public function setHitType($type) {
    if (!in_array($type, ['pageview', 'screenview', 'event', 'transaction', 'item', 'social', 'exception', 'timing'])) {
      throw new \InvalidArgumentException('Hit type not valid.');
    }
    $this->dimensions['hit_type'] = $type;
    return $this;
  }

  /**
   * Category for the event
   *
   * @param string $category
   *
   * @return Event
   */
  public function setEventCategory($category) {
    $this->validateLength($category, 150);
    $this->dimensions['event_category'] = $category;
    return $this;
  }

  /**
   * Action for the event
   *
   * @param string $action
   * @return Event
   */
  public function setEventAction($action) {
    $this->validateLength($action, 500);
    $this->dimensions['event_action'] = $action;
    return $this;
  }

  /**
   * Label for the event
   *
   * @param string $label
   * @return Event
   */
  public function setEventLabel($label) {
    $this->validateLength($label, 500);
    $this->dimensions['event_label'] = $label;
    return $this;
  }

  /**
   * Value of the event
   *
   * @param int $value
   * @return Event
   */
  public function setEventValue($value) {
    if (!is_int($value)) {
      throw new \InvalidArgumentException("Event value must be an integer.");
    }
    $this->dimensions['event_value'] = $value;
    return $this;
  }

  /**
   * Set the value for a custom dimension.
   *
   * @param string $name
   * @param string $value
   */
  public function setCustomDimension($name, $value) {
    $this->custom_dimensions[$name] = $value;
  }

  /**
   * Return an array of data ready
   * to send to the client script.
   *
   * @return array
   */
  public function toArray(EventService $service) {
    return [
      'id' => $this->id,
      'dimensions' => array_merge($this->dimensions, $service->getGlobalDimensions())
      ];
  }
}
