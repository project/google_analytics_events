<?php

namespace Drupal\google_analytics_events;

use Drupal\google_analytics_events\Event as GaEvent;

/**
 * Service for managing event registration.
 */
class EventService {

  #region Drupal 7 compatibility

  /**
   * Service instancce
   *
   * @var EventService
   */
  private static $instance;

  /**
   * Summary of getInstance
   * @return EventService
   */
  public static function getInstance() {
    if (empty(static::$instance)) {
      static::$instance = new EventService();
    }
    return static::$instance;
  }

  #endregion

  function __construct() {

  }

  /**
   * Storage for global (requested to scope) dimensions
   * that should be attached to all events.
   *
   * @var mixed[]
   */
  protected $dimensions = [];

  /**
   * Array of events
   *
   * @var GaEvent[]
   */
  protected $events = [];

  /**
   * Add an event to the queue, to be
   * added to drupal settings somewhere before
   * the end of this request.
   *
   * @param GaEvent $event
   */
  public function queueEvent(Event $event) {
    $this->events[] = $event;
  }

  /**
   * Attach the event to a form element. Carefull with usage,
   * on cached content the triggering of these events will
   * also be cached.
   *
   * @param GaEvent $event
   * @param array $form_element
   */
  public function attachEvent(Event $event, array &$form_element) {
    $form_element['#attached']['js'][] = [
      'data' => ['google_analytics_events' => ['events' => [(array) $event]]],
      'type' => 'setting'
    ];
  }

  /**
   * If there are any events in the queue.
   *
   * @return bool
   */
  public function hasEvents() {
    return isset($this->events[0]);
  }

  /**
   * Return the current list of events.
   *
   * @return GaEvent[]
   */
  public function popEvents() {
    $events = $this->events;
    $this->events = [];
    return $events;
  }

  /**
   * Adds a global dimension to the current request scope.
   *
   * This dimensions will be attached to all events
   * during the current request.
   *
   * @param string $name
   * @param string $value
   */
  public function addGlobalDimension($name, $value) {
    $this->dimensions[$name] = $value;
  }

  /**
   * Get an array of the current global dimensions.
   * 
   * @return mixed[]
   */
  public function getGlobalDimensions() {
    return $this->dimensions;
  }
}
