﻿(function ($) {
  "use strict";
  Drupal.behaviors.google_analytics_events = {
    attach: function (context, $settings) {

      // If there is no google analytics, do nothing...
      if (typeof (ga) != 'function') {
        return;
      }

      var $has = $settings.google_analytics_events && $settings.google_analytics_events.events;
      if (!$has) {
        return;
      }

      var $events = $settings.google_analytics_events.events;

      $.each($events, function (index, event) {
        var $dimensions = event.dimensions;
        ga('send', $dimensions);
      });

      // No need to keep these, that will get sent again
      // during the next attach if not removed.
      $settings.google_analytics_events.events = [];
    }
  };
}(jQuery));